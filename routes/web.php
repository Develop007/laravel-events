<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('index');

Route::get('/event/{id}', 'EventController@index')->name('event');
Route::get('/event/join/{id}', 'EventController@join')->middleware('auth')->name('event-join');
Route::get('/event/leave/{id}', 'EventController@leave')->middleware('auth')->name('event-leave');

// Мои Мероприятия
Route::get('/my-events', 'EventController@my_events')->middleware('auth')->name('my-events');
Route::post('/event-create-form', 'EventController@event_create_form')->middleware('auth')->name('event-create-form');
Route::get('/event-create', 'EventController@event_create')->middleware('auth')->name('event-create');
Route::get('/event-delete', 'EventController@event_delete')->middleware('auth')->name('event-delete');
Route::post('/event-edit-form', 'EventController@event_edit_form')->middleware('auth')->name('event-edit-form');
Route::get('/event-edit/{id}', 'EventController@event_edit')->middleware('auth')->name('event-edit');
// Комментарии
Route::post('/comment-form', 'EventController@comment_form')->middleware('auth')->name('comment-form');

// Мои Мониторы
Route::get('/monitor/list', 'MonitorController@list')->middleware('auth')->name('monitor-list');
Route::post('/monitor/create-form', 'MonitorController@create_form')->middleware('auth')->name('monitor-create-form');
Route::get('/monitor/create', 'MonitorController@create')->middleware('auth')->name('monitor-create');
Route::get('/monitor/delete', 'MonitorController@delete')->middleware('auth')->name('monitor-delete');
Route::post('/monitor/edit-form', 'MonitorController@edit_form')->middleware('auth')->name('monitor-edit-form');
Route::get('/monitor/edit/{id}', 'MonitorController@edit')->middleware('auth')->name('monitor-edit');
Route::get('/monitor/{id}', 'MonitorController@index')->middleware('auth')->name('monitor');
Route::get('/monitor/getMessage/{id}', 'MonitorController@getMessage')->middleware('auth')->name('monitor-getMessage');

// Админ-панель
Route::get('/admin', 'AdminController@events')->middleware('admin')->name('admin');
Route::get('/admin/events', 'AdminController@events')->middleware('admin')->name('admin-events');
Route::get('/admin/monitors', 'AdminController@monitors')->middleware('admin')->name('admin-monitors');
// Пользователи
Route::get('/admin/users', 'AdminController@users')->middleware('admin')->name('admin-users');
Route::get('/admin/user/delete/{id}', 'AdminController@user_delete')->middleware('admin')->name('admin-user-delete');