<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->bigInteger('creator_id')->unsigned();
            $table->boolean('isActive');
            $table->integer('min_member')->comment('Минимум участников');
            $table->integer('max_member')->comment('Максимум участников');
            $table->integer('member_list_id')->comment('Список участников мероприятия');
            $table->timestamp('start_date')->nullable()->comment('Начало мероприятия');
            $table->timestamp('end_date')->nullable()->comment('Конец мероприятия');
            $table->timestamp('start_bid_date')->nullable()->comment('Начало приема заявок');
            $table->timestamp('end_bid_date')->nullable()->comment('Конец приема заявок');
            $table->timestamps();

            $table->foreign('creator_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
