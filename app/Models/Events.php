<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $fillable = ['name', 'description', 'min_member', 'max_member', 'start_date', 'end_date', 'start_bid_date', 'end_bid_date', 'created_at', 'updated_at'];
	protected $dates = ['start_date', 'end_date', 'start_bid_date', 'end_bid_date', 'created_at', 'updated_at'];
}
