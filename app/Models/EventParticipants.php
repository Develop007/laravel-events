<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventParticipants extends Model
{    
    public $timestamps = false;
    
    public function user(){
        return $this->hasOne('App\User', 'id', 'creator_id');
    }
}
