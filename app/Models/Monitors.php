<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Monitors extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'message', 'event_id'];
}
