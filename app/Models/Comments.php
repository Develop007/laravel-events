<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
	protected $dates = ['created_at', 'updated_at'];

    public function user(){
        return $this->hasOne('App\User', 'id', 'creator_id');
    }
}
