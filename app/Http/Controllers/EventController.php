<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Events;
use App\Models\EventParticipants;
use App\Models\Comments;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    public function index(Request $request)
    {
        $event = Events::where('id', $request->id)->where('isActive', 1)->first();
        $isJoined = EventParticipants::where('event_id', $request->id)->where('user_id', Auth::id())->exists();

        $comments = Comments::where('event_id', $event->id)->get();

        $participants = null;
        if($event->creator_id == Auth::id())
            $participants = EventParticipants::join('users', 'users.id', '=', 'event_participants.user_id')->where('event_id', $request->id)->get();

        return view('event', [
            'event'         => $event,
            'isJoined'      => $isJoined,
            'participants'  => $participants,
            'comments'      => $comments,
        ]);
    }

    public function join(Request $request)
    {
        $event = new EventParticipants;

        $event->event_id = $request->id;
        $event->user_id = Auth::id();

        $event->save();

        return redirect()->route('event', ['id' => $request->id]);
    }

    public function leave(Request $request)
    {
        $event = EventParticipants::where('event_id', $request->id)->where('user_id', Auth::id());

        $event->delete();

        return redirect()->route('event', ['id' => $request->id]);
    }


    public function my_events()
    {
        $events = EventParticipants::rightJoin('events', 'events.id', '=', 'event_participants.event_id')->where('user_id', Auth::id())->orWhere('creator_id', Auth::id())->get();


        return view('my-events', [
            'events' => $events,
        ]);
    }

    public function event_create_form(Request $request)
    {
        $event = new Events();

        $event->fill($request->all());
        $event->creator_id = Auth::id();
        $event->isActive = 1;
        $event->member_list_id = 0;

        $event->save();


        return redirect()->route('event', ['id' => $event->id]);
    }

    public function event_create(Request $request)
    {

        return view('event_create');
    }


    public function event_edit(Request $request)
    {

        $event = Events::find($request->id);

        return view('event_edit', ['event' => $event]);
    }


    public function event_edit_form(Request $request)
    {
        $event = Events::find($request->id);

        $event->fill($request->all());

        $event->update();


        return redirect()->route('my-events');
    }


    public function event_delete(Request $request)
    {
        if(Auth::user()->isAdmin)
            $event = Events::where('id', $request->id);
        else
            $event = Events::where('id', $request->id)->where('creator_id', Auth::id());

        $event->delete();

        return redirect()->back();
    }


    public function comment_form(Request $request)
    {
        $comment = new Comments();

        $comment->comment = $request->comment;
        $comment->creator_id = Auth::id();
        $comment->event_id = $request->event_id;

        $comment->save();


        return redirect()->route('event', ['id' => $comment->event_id]);
    }
}
