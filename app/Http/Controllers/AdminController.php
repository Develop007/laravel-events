<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Events;
use App\Models\EventParticipants;
use App\Models\Comments;
use App\Models\Monitors;
use App\User;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index()
    {

        return view('admin.index');
    }

    public function events()
    {
        $events = EventParticipants::rightJoin('events', 'events.id', '=', 'event_participants.event_id')->get();


        return view('admin.events', [
            'events' => $events,
        ]);
    }

    public function monitors()
    {
    	$monitors = Monitors::select(['monitors.*', 'events.name AS event_name'])
    		->leftJoin('events', 'events.id', '=', 'monitors.event_id')
    		->get();

		$events = Events::all();

        return view('admin.monitors', [
            'monitors' => $monitors,
        ]);
    }

    public function users()
    {
        $users = User::all();


        return view('admin.users', [
            'users' => $users,
        ]);
    }

    public function user_delete(Request $request)
    {
        $user = User::where('id', $request->id);

        $user->delete();

        return redirect()->back();
    }
}
