<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Events;
use App\Models\Monitors;
use Illuminate\Support\Facades\Auth;

class MonitorController extends Controller
{
    public function index(Request $request)
    {
    	$monitor = 	Monitors::find($request->id);
    	$message = '';
    	if(isset($monitor->message))
        	$message = str_replace('\n', '<br>', $monitor->message);

        return view('monitor.monitor', [
            'id' => $request->id,
            'message' => $message,
        ]);
    }

    public function list()
    {
    	$monitors = Monitors::select(['monitors.*', 'events.name AS event_name'])
    		->leftJoin('events', 'events.id', '=', 'monitors.event_id')
    		->where('creator_id', Auth::id())
    		->where('isActive', 1)
    		->get();

        return view('monitor.list', [
            'monitors' => $monitors,
        ]);
    }


    public function create()
    {
    	$monitor = new Monitors();
    	$events = Events::where('creator_id', Auth::id())->get();

        return view('monitor.create', [
            'monitor' => $monitor,
            'events' => $events,
        ]);
    }

    public function create_form(Request $request)
    {
        $monitor = new Monitors();

        // Является ли пользователь владельцем мероприятия
    	$isAccessEvent = Events::where('creator_id', Auth::id())->where('id', $request->event_id)->exists();
    	if(!$isAccessEvent)
			abort(403);


        $monitor->fill($request->all());
        $monitor->message = $request->message ?? '';

        $monitor->save();


        return redirect()->route('monitor-list');
    }

    public function edit(Request $request)
    {
    	$monitor = Monitors::find($request->id);
        if(Auth::user()->isAdmin)
           $events = Events::all();
       else
    	   $events = Events::where('creator_id', Auth::id())->get();

        return view('monitor.edit', [
            'monitor' => $monitor,
            'events' => $events,
        ]);
    }

    public function edit_form(Request $request)
    {
        $monitor = Monitors::find($request->id);

        // Является ли пользователь владельцем мероприятия
    	$isAccessEvent = Events::where('creator_id', Auth::id())->where('id', $request->event_id)->exists();
    	if(!$isAccessEvent)
			abort(403);


        $monitor->fill($request->all());
        $monitor->message = $request->message ?? '';

        $monitor->update();


        return redirect()->route('monitor-list');
    }

    public function delete(Request $request)
    {
        $monitors = Monitors::where('id', $request->id)->first();
        $isAccessEvent = Events::where('id', $monitors->event_id)->where('creator_id', Auth::id())->exists();
        if(!$isAccessEvent)
			abort(403);

        $monitors->delete();

        return redirect()->route('monitor-list');
    }

    public function getMessage(Request $request)
    {
		if(!$request->ajax())
			abort(404);

    	$monitor = 	Monitors::find($request->id);
        $message = str_replace('\n', '<br>', $monitor->message);

        return $message;
    }
}
