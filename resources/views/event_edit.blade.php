@extends('layouts.app')


@section('content')

<h2>Обновление мероприятия</h2>

<br>
<form method="POST" action="{{ route('event-edit-form') }}">
	@csrf
	<input type="hidden" name="id" value="{{ $event->id }}">

	<div class="form-group row">
		<label for="name" class="col-md-3 col-form-label text-right">Название</label>
		<div class="col-md-7">
			<input type="text" name="name" class="form-control" value="{{ $event->name }}" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="description" class="col-md-3 col-form-label text-right">Описание</label>
		<div class="col-md-7">
			<textarea name="description" class="form-control" required>{{ $event->description }}</textarea>
		</div>
	</div>
	<div class="form-group row">
		<label for="min_member" class="col-md-3 col-form-label text-right">Минимум участников</label>
		<div class="col-md-7">
			<input type="number" name="min_member" class="form-control" value="{{ $event->min_member }}" min=1 value=1 required>
		</div>
	</div>
	<div class="form-group row">
		<label for="max_member" class="col-md-3 col-form-label text-right">Максимум участников</label>
		<div class="col-md-7">
			<input type="number" name="max_member" class="form-control" value="{{ $event->max_member }}" min=1 value=100 required>
		</div>
	</div>
	<div class="form-group row">
		<label for="start_date" class="col-md-3 col-form-label text-right">Начало мероприятия</label>
		<div class="col-md-7">
			<input type="date" name="start_date" class="form-control" value="{{ isset($event->start_date) ? $event->start_date->format('Y-m-d') : null }}" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="end_date" class="col-md-3 col-form-label text-right">Конец мероприятия</label>
		<div class="col-md-7">
			<input type="date" name="end_date" class="form-control" value="{{ isset($event->end_date) ? $event->end_date->format('Y-m-d') : null }}" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="start_bid_date" class="col-md-3 col-form-label text-right">Начало приема заявок</label>
		<div class="col-md-7">
			<input type="date" name="start_bid_date" class="form-control" value="{{ isset($event->start_bid_date) ? $event->start_bid_date->format('Y-m-d') : null }}" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="end_bid_date" class="col-md-3 col-form-label text-right">Конец приема заявок</label>
		<div class="col-md-7">
			<input type="date" name="end_bid_date" class="form-control" value="{{ isset($event->end_bid_date) ? $event->end_bid_date->format('Y-m-d') : null }}" required>
		</div>
	</div>

<div class="text-center">
	<button class="btn btn-success">Обновить мероприятие</button>
</div>
</form>
@endsection