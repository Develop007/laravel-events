@extends('layouts.app')


@section('content')

      <div class="jumbotron">
        <div class="container">
          <h1 class="display-4">Добро пожаловать!</h1>
          <p class="lead">На данном сайте размещена информация по мероприятиям.</p>
          <hr class="my-4">
          @isset( $events[0]->name )
            <h3>{{ $events[0]->name }}</h3>
            <p>{{ $events[0]->description }}</p>
            <p class="lead">
              <a class="btn btn-primary btn-md" href="{{ route('event', [ 'id' => $events[0]->id ]) }}" role="button">Подробнее »</a>
            </p>
          @endisset
        </div>
      </div>




      <div class="container">
        <div class="row">
          @foreach($events as $event)
            <div class="col-md-4">
              <h2>{{ $event->name }}</h2>
              <p>{{ $event->description }}</p>
              <p><a class="btn btn-secondary" href="{{ route('event', [ 'id' => $event->id ]) }}" role="button">Подробнее »</a></p>
            </div>
          @endforeach
        </div>

        <hr>

      </div>

@endsection