@extends('layouts.app')


@section('content')

@isset($event)
	<h1>{{ $event->name }}</h1>
	@if(isset($event->start_date) && isset($event->end_date))
		<p class="lead">Время проведения: {{ $event->start_date->format('H:i d-m-Y') }} - {{ $event->end_date->format('H:i d-m-Y') }}</p>
	@endisset
	<p>{{ $event->description }}</p>

	@if(isset($event->end_date->timestamp) && $event->end_date->timestamp <= time())
		<div class="alert alert-warning" role="alert">
		  Мероприятие уже завершено!
		</div>
	@elseif(isset($event->end_bid_date->timestamp) && $event->end_bid_date->timestamp <= time())
		<div class="alert alert-secondary" role="alert">
		  Время приема заявок истекло
		</div>
	@else
		@if(!$isJoined)
			<a class="btn btn-success" href="{{ route('event-join', ['id' => $event->id]) }}" role="button">Присоединиться</a>
		@else
			<a class="btn btn-danger" href="{{ route('event-leave', ['id' => $event->id]) }}" role="button">Не участвовать</a>
		@endif
	@endif

	<br><br>
	@if(isset($participants) && !$participants->isEmpty())
		<h4 class="text-center">Участники:</h4>
		<table class="table text-center">
  			<thead class="thead-dark">
				<tr>
					<th>
						Имя
					</th>
					<th>
						Пол
					</th>
				</tr>
  			</thead>
  			<tbody>
				@foreach($participants as $participant)
					<tr>
						<td>
							{{ $participant->name }}
						</td>
						<td>
							{{ $participant->gender ? 'Мужской' : 'Женский' }}
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	@endif

	<br>
	<h4>Комментарии</h4>
	@auth
		<form method="POST" action="{{ route('comment-form') }}">
			@csrf
			<input type="hidden" name="event_id" value="{{ $event->id }}">
			<div class="form-group">
				<textarea name="comment" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<button class="btn btn-primary">Отправить</button>
			</div>
		</form>
	@endauth

	@foreach($comments as $comment)
		<div class="card">
		  <div class="card-body">
		    <h5 class="card-title">
		    	{{ $comment->user->name }}
		    </h5>
		    <p class="card-text">{{ $comment->comment }}</p>
		    <p class="card-text text-right"><small class="text-muted">{{ $comment->created_at->format('H:i d-m-Y') }}</small></p>
		  </div>
		</div>
		<br>
	@endforeach

@else
    <div class="alert alert-danger" role="alert">
    	Мероприятие не найдено
    </div>
@endisset

@endsection