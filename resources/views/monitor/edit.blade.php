@extends('layouts.app')


@section('content')

<h2>Создание монитора</h2>

<br>
<form method="POST" action="{{ route('monitor-edit-form') }}">
	@csrf
	<input type="hidden" name="id" value="{{ $monitor->id }}">

	<div class="form-group row">
		<label for="name" class="col-md-3 col-form-label text-right">Название</label>
		<div class="col-md-7">
			<input type="text" name="name" class="form-control" value="{{ $monitor->name }}" required>
		</div>
	</div>

	<div class="form-group row">
		<label for="message" class="col-md-3 col-form-label text-right">Сообщение</label>
		<div class="col-md-7">
			<input type="text" name="message" class="form-control" value="{{ $monitor->message }}">
		</div>
	</div>


	<div class="form-group row">
		<label for="event_id" class="col-md-3 col-form-label text-right">Мероприятие</label>
		<div class="col-md-7">
			<select class="form-control" name="event_id" required>
                <option value disabled>Выберите меропритие</option>
                @foreach($events as $event)
					<option value="{{ $event->id }}" @if($monitor->event_id == $event->id) {{ 'selected' }} @endif }}>
						{{ $event->name }}
					</option>
				@endforeach
			</select>
		</div>
	</div>

<div class="text-center">
	<button class="btn btn-success">Обновить монитор</button>
</div>
</form>
@endsection