@extends('layouts.app')


@section('content')

<h2>Создание монитора</h2>

<br>
<form method="POST" action="{{ route('monitor-create-form') }}">
	@csrf

	<div class="form-group row">
		<label for="name" class="col-md-3 col-form-label text-right">Название</label>
		<div class="col-md-7">
			<input type="text" name="name" class="form-control" required>
		</div>
	</div>

	<div class="form-group row">
		<label for="message" class="col-md-3 col-form-label text-right">Сообщение</label>
		<div class="col-md-7">
			<input type="text" name="message" class="form-control">
		</div>
	</div>


	<div class="form-group row">
		<label for="event_id" class="col-md-3 col-form-label text-right">Мероприятие</label>
		<div class="col-md-7">
			<select class="form-control" name="event_id" required>
                <option value disabled selected>Выберите меропритие</option>
                @foreach($events as $event)
					<option value="{{ $event->id }}">
						{{ $event->name }}
					</option>
				@endforeach
			</select>
		</div>
	</div>

<div class="text-center">
	<button class="btn btn-success">Создать монитор</button>
</div>
</form>
@endsection