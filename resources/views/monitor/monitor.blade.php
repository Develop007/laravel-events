<!DOCTYPE html>
<html>
<head>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    	<title>
		    @if(View::hasSection('title'))
		        @yield('title')
		    @else
		        {{ config('app.name') }}
		    @endif
		</title>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

		<style type="text/css">
			.message-center{
			    display: flex;
			    align-items: center;
			    justify-content: center;
			    height: 95vh;
			}
		</style>

</head>
<body>
      	
      	<h1 style="font-size:172px;">
      		<div id="ajax-message" class="message-center">{{ $message }}</div>
      	</h1>


<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script type="text/javascript">
	setInterval(
		() => (
			$.ajax({
			  type: "GET",
			  url: "{{ route('monitor-getMessage', ['id' => $id ]) }}",
			  success: function(msg){
				$('#ajax-message').html(msg);
			  }
			})
		), 2000
	);
</script>
</body>
</html>