@extends('layouts.app')


@section('content')

<div class="alert alert-secondary" role="alert">
  	На мониторах размещается обновляемая информация. Они позволят вам быстро уведомить пользователя о чем-либо.
</div>

<div class="row">
	<div class="col text-right">
		<a href="{{ route('monitor-create') }}" class="btn btn-success">Создать монитор</a>
	</div>
</div>
<br>
<table class="table text-center">
	<thead>
		<tr>
			<th>
				Название
			</th>
			<th>
				Сообщение
			</th>
			<th>
				Мероприятие
			</th>
			<th>
				
			</th>
		</tr>
	</thead>
	<tbody>
		@foreach($monitors as $monitor)
			<tr>
				<td>
					{{ $monitor->name }}
				</td>
				<td>
					{{ $monitor->message }}
				</td>
				<td>
					{{ $monitor->event_name }}
				</td>
				<td>
					<a class="btn btn-info btn-sm" href="{{ route('monitor', ['id' => $monitor->id]) }}" role="button">Открыть</a>
					<a class="btn btn-primary btn-sm" href="{{ route('monitor-edit', ['id' => $monitor->id]) }}" role="button">Изменить</a>
					<a class="btn btn-danger btn-sm" href="{{ route('monitor-delete', ['id' => $monitor->id]) }}" role="button">X</a>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>



@endsection