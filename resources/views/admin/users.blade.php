@extends('layouts.app')


@section('content')
@include('admin.inc_menu')

<br><br>
<h3 class="text-center">Пользователи:</h3>
<table class="table">
	<tr>
		<th>
			Пользователь
		</th>
		<th>
			E-mail
		</th>
		<th>
			Дата рождения
		</th>
		<th>
			Пол
		</th>
		<th>
			Дата регистрации
		</th>
		<th>
			
		</th>
	</tr>
	@foreach($users as $user)
		<tr>
			<td>
				{{ $user->name }}
			</td>
			<td>
				{{ $user->email }}
			</td>
			<td>
				@isset($user->birth_day)
					{{ $user->birth_day->format('d-m-Y') }}
				@else
					Не задано
				@endisset
			</td>
			<td>
				{{ $user->gender ? 'Мужской' : 'Женский' }}
			</td>
			<td>
				@isset($user->create_at)
					{{ $user->create_at->format('d-m-Y') }}
				@else
					Не задано
				@endisset
			</td>
			<td>
				<a class="btn btn-danger btn-sm" href="{{ route('admin-user-delete', ['id' => $user->id]) }}" role="button">X</a>
			</td>
		</tr>
	@endforeach
</table>

@endsection