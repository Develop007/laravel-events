@extends('layouts.app')


@section('content')
@include('admin.inc_menu')

<div class="text-right">
	<a href="{{ route('event-create') }}" class="btn btn-success">Создать мероприятие</a>
</div>

<br>
@if(!$events->isEmpty())
	<table class="table">
		<tr>
			<th>
				Название мероприятия
			</th>
			<th>
				Дата начала
			</th>
			<th>
				Дата окончания
			</th>
			<th>
				Организатор
			</th>
			<th class="text-center">
				
			</th>
		</tr>
		@foreach($events as $event)
			<tr>
				<td>
					{{ $event->name }}
				</td>
				<td>
					@isset($event->start_date)
						{{ $event->start_date }}
					@else
						Не задано
					@endisset
				</td>
				<td>
					@isset($event->end_date)
						{{ $event->end_date }}
					@else
						Не задано
					@endisset
				</td>
				<td>
					{{ $event->user->name }}
				</td>
				<td class="text-center">
					<a href="{{ route('event', [ 'id' => $event->id ]) }}" class="btn btn-info btn-sm">Подробности</a>
					@isset($event->event_id)
						<a class="btn btn-danger btn-sm" href="{{ route('event-leave', ['id' => $event->id]) }}" role="button">Не участвовать</a>
					@else
						<a class="btn btn-success btn-sm" href="{{ route('event-join', ['id' => $event->id]) }}" role="button">Присоединиться</a>
					@endisset
					<a class="btn btn-primary btn-sm" href="{{ route('event-edit', ['id' => $event->id]) }}" role="button">Изменить</a>
					<a class="btn btn-danger btn-sm" href="{{ route('event-delete', ['id' => $event->id]) }}" role="button">X</a>
				</td>
			</tr>
		@endforeach
	</table>
@else
	<h3 class="text-center">Список мероприятий пуст :(</h3>
@endif

@endsection