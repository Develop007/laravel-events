<!DOCTYPE html>
<html>
<head>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    	<title>
		    @if(View::hasSection('title'))
		        @yield('title')
		    @else
		        {{ config('app.name') }}
		    @endif
		</title>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>

	@include('inc.header')

    <main role="main">


      <div class="container mt-4 mb-4">
      	@yield('content')
      </div>


    </main>


	@include('inc.footer')

	@include('inc.post_scripts')
  

</body>
</html>