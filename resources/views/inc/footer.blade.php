    <footer class="footer container">
      <p>© {{ config('app.name') }} {{ date('Y') }}</p>
    </footer>